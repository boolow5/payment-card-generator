package main

import (
	"flag"
	"fmt"
	"image"
	"io/ioutil"
	"os"
	"path/filepath"
	"time"

	"bitbucket.org/boolow5/Kiro/payment-card-generator/config"
	"bitbucket.org/boolow5/Kiro/payment-card-generator/models"
)

var (
	count               int
	expireInHowManyDays int
	outputFile          string
	noJSON              bool
	fromJSON            bool
	generateQR          bool
	qrImageDirectory    string
	currencyCode        string
	currencySymbol      string
	amount              float64
	qrImageTemplate     string
	baseURL             string
	offsetX             int
	offsetY             int
	overWrite           bool
)

func init() {
	parseFlags()
}

func main() {
	hours := time.Duration(24*expireInHowManyDays) * time.Hour
	exprationDate := time.Now().Add(hours)
	fmt.Printf("Generating %d keys which will be valid until (hours: %v) %v (or %d days)\nThe output will be written to '%s'\n", count, hours, exprationDate, expireInHowManyDays, outputFile)
	fmt.Printf("\t -no-json='%v'\t-generate-qr-code='%v'\t-qr-dir='%v'\n", noJSON, generateQR, qrImageDirectory)
	fmt.Printf("\t -currency-code='%v'\t-currency-symbol='%v'\t-amount='%v'\n", currencyCode, currencySymbol, amount)

	models.BaseURL = baseURL

	defaultAmount, defaultCurrency, err := models.GetDataFromTemplateImage(qrImageTemplate)
	if err == nil && defaultAmount > 0 && defaultCurrency != "" {
		currencyCode = defaultCurrency
		amount = defaultAmount
		fmt.Printf("\nUsing default values from qr template file name %v\n\tCurrency: %v\tAmount: %v\n", qrImageTemplate, defaultCurrency, defaultAmount)
	} else {
		fmt.Printf("\nNot using default values: Error: %v\n\tTemplate file: %v\n\tCurrency: %v\tAmount: %v\n", err, qrImageTemplate, defaultCurrency, defaultAmount)
	}

	cards := []models.Card{}

	if !fromJSON {
		fmt.Printf("Generating %d new cards data\n", count)
		for i := 0; i < count; i++ {
			card, err := models.GenerateNewCard(exprationDate, amount, currencyCode, currencySymbol, config.Get().SecretKey)
			if err != nil {
				fmt.Printf("[%d] generating new card failed: %v\n", i, err)
				continue
			}
			fmt.Printf("[%d]\t%v\n", i, card.String())
			cards = append(cards, card)
		}
	} else {
		fmt.Printf("Reading JSON data from %s\n", outputFile)
		data, err := ioutil.ReadFile(outputFile)
		if err != nil {
			panic(err)
		}
		cards, err = models.ParseCards(data)
		if err != nil {
			panic(err)
		}
	}

	if !noJSON {
		fmt.Printf("Saving JSON data to %s\n", outputFile)
		oldCards := []models.Card{}
		if overWrite {
			data, err := ioutil.ReadFile(outputFile)
			if err != nil {
				panic(err)
			}
			oldCards, err = models.ParseCards(data)
			if err != nil {
				panic(err)
			}

			cards = append(cards, oldCards...)
		}
		data, err := models.CardsToJSON(cards)
		if err != nil {
			panic(err)
		}

		err = ioutil.WriteFile(outputFile, data, os.FileMode(0666))
		if err != nil {
			panic(err)
		}
	}

	if generateQR {
		fmt.Printf("Generating QR code images in '%s'\n", qrImageDirectory)
		fileName := ""
		var err error
		qrCode := models.QRCode{}
		for _, card := range cards {
			qrCode = card.ToQRCode()
			fileName = filepath.Join(qrImageDirectory, fmt.Sprintf("%s-%.0f-%d.png", card.CurrencyCode, card.Amount, card.UniqueNumber))
			err = qrCode.SaveToFile(fileName, image.Pt(offsetX, offsetY), qrImageTemplate, card.ExpiresAt.Format("02 Jan 2006"))
			if err != nil {
				fmt.Printf("[QR CODE] generating qr code file failed: %v\n", err)
			} else {
				fmt.Printf("[QR CODE] saved to %s\n", fileName)
			}
		}
	} else {
		fmt.Printf("QR code images are not generated. To generate them -generate-qr-code to true now set to %v\n", generateQR)
	}

	fmt.Printf("Done!")
}

func parseFlags() {
	flag.StringVar(&outputFile, "output-file", "./output.json", "File that will be hodling the generated keys")
	flag.IntVar(&count, "count", 10, "The number of keys that will be generated, if not passed 10 is the default count")
	flag.IntVar(&expireInHowManyDays, "expires-in", 90, "The number of days the generated keys will be valid")
	flag.BoolVar(&noJSON, "no-json", false, "If you don't want to save the output to json file")
	flag.BoolVar(&fromJSON, "from-json", false, "Do not generate new cards but use previously generated data from the json file")
	flag.BoolVar(&generateQR, "generate-qr-code", false, "If you want to generate the qr code images")
	flag.StringVar(&qrImageDirectory, "qr-dir", "./qr-codes", "If -generate-qr-code is true this will be used to store generated qr code images")
	flag.StringVar(&currencyCode, "currency-code", "KES", "Currency code of the generated cards")
	flag.StringVar(&currencySymbol, "currency-symbol", "KSh", "Currency symbol of the generted cards")
	flag.Float64Var(&amount, "amount", 100, "The amount the card represents")
	flag.StringVar(&qrImageTemplate, "qr-template", "", "QR Template is an image that is used to add the QR code to")
	flag.StringVar(&baseURL, "base-url", "https://kiro.iweydi.com", "The base url and the domain used to construct the QR url")
	flag.IntVar(&offsetX, "offset-x", 25, "The horizontal offset of the QR code on the template image")
	flag.IntVar(&offsetY, "offset-y", 150, "The vertical offset of the QR code on the template image")
	flag.BoolVar(&overWrite, "overwrite", false, "Overwrite data in [output].json file")

	flag.Parse()
}

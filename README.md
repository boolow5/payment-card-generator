# Payment Card Generator

automatically parses and generates payment cards with template provided through the parameter.

# Running Script

```bash
# build the script
go build .

# run the executable
./payment-card-generator -generate-qr-code=1 -expires-in 90 -qr-dir ./qr-codes -qr-template ./qr-templates/prepaid-card-1000-ksh-template.png
```
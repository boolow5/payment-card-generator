package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"path/filepath"
)

var (
	config   Configuration
	confPath string
)

// Configuration holds all config data for the project
type Configuration struct {
	SecretKey string `json:"secret_key"`
}

func init() {
	confPath = "./"

	if err := Load(confPath); err != nil {
		fmt.Println("Cannot load config file reason:", err)
		panic(err)
	}
	fmt.Println("Initialized configuration.")
}

// Get returns config local variable
func Get() Configuration {
	return config
}

// Load reads the config data from the json file
func Load(confPath string) error {
	fmt.Printf("Load (confPath: %s)\n", confPath)
	file, err := ioutil.ReadFile(filepath.Join(confPath, "config.json"))
	if err != nil {
		return err
	}
	return json.Unmarshal(file, &config)
}

// GetPath returns directory which contains the config.json file
func GetPath() string {
	return confPath
}

package models

import (
	"encoding/json"
	"fmt"
	"time"

	"bitbucket.org/boolow5/Kiro/payment-card-generator/utils"
	"gopkg.in/mgo.v2/bson"
)

var (
	BaseURL = ""
)

// Card represents a scrach-card like the telephone companies use to recharge airtime
type Card struct {
	ID             bson.ObjectId `json:"id" bson:"_id"`
	UniqueNumber   int64         `json:"unique_number" bson:"unique_number"`
	ExpiresAt      time.Time     `json:"expires_at" bson:"expires_at"`
	Signkey        string        `json:"-" bson:"sign_key"`
	Amount         float64       `json:"amount" bson:"amount"`
	CurrencyCode   string        `json:"currency_code" bson:"currency_code"`
	CurrencySymbol string        `json:"currency_symbol" bson:"currency_symbol"`

	CreatedAt time.Time `json:"created_at" bson:"created_at"`
	Updated   time.Time `json:"updated_at" bson:"updated_at"`
}

// String returns a string constracted from the data in card
func (c Card) String() string {
	return fmt.Sprintf("<Card id: %s. unique number: %d. expires: %v. created: %v. updated: %v>", c.ID.Hex(), c.UniqueNumber, c.ExpiresAt.Format(time.RFC3339), c.CreatedAt.Format(time.RFC3339), c.Updated.Format(time.RFC3339))
}

// SetUnqueNumber sets unique number of the card
func (c *Card) SetUnqueNumber() {
	uniqueNumber := utils.HashUInt32(c.ID.Hex())
	c.UniqueNumber = int64(uniqueNumber)
}

// ToKey returns a string constracted from the data in card
func (c Card) ToKey() string {
	return fmt.Sprintf("%s.%d.%v.%v.%v", c.ID.Hex(), c.UniqueNumber, c.ExpiresAt.Format(time.RFC3339), c.CreatedAt.Format(time.RFC3339), c.Updated.Format(time.RFC3339))
}

// ToQRCode returns a qr card object
func (c Card) ToQRCode() QRCode {
	qr := QRCode{RequestType: "payment_card", ItemID: c.ID, BaseURL: BaseURL}
	if qr.ExtraParameters == nil {
		qr.ExtraParameters = map[string]string{}
	}
	// qr.ExtraParameters["expires_at"] = c.ExpiresAt.Format(time.UnixDate)
	// qr.ExtraParameters["sign_key"] = c.Signkey
	qr.ExtraParameters["unique_number"] = fmt.Sprint(c.UniqueNumber)
	return qr
}

// CardsToJSON marshals cards slice into byte slice
func CardsToJSON(cards []Card) ([]byte, error) {
	return json.Marshal(&cards)
}

// ParseCards unmarshals slice of bytes to cards slice
func ParseCards(data []byte) ([]Card, error) {
	cards := []Card{}
	err := json.Unmarshal(data, &cards)
	return cards, err
}

// GenerateNewCard generates new card from expiration date and secret key
func GenerateNewCard(expirationDate time.Time, amount float64, currencyCode, currencySymol, key string) (card Card, err error) {
	if expirationDate.IsZero() {
		return card, fmt.Errorf("expiration date is zero")
	}
	if expirationDate.Before(time.Now()) {
		return card, fmt.Errorf("expiration date is already passed")
	}

	now := time.Now()

	if currencyCode == "" {
		currencyCode = "KES"
		currencySymol = "KSh"
	}

	card.ID = bson.NewObjectId()
	card.ExpiresAt = expirationDate
	card.CreatedAt = now
	card.Updated = now
	card.CurrencyCode = currencyCode
	card.CurrencySymbol = currencySymol
	card.Amount = amount
	card.SetUnqueNumber()

	card.Signkey = utils.Hash(card.ToKey(), key)

	return card, nil
}

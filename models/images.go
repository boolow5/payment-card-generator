package models

import (
	"fmt"
	"image"
	"image/draw"
	"image/jpeg"
	"log"
	"os"
	"strconv"
	"strings"

	"github.com/fogleman/gg"
)

// OpenImage takes file name and returns image, format and error
func OpenImage(file string) (image.Image, string, error) {
	imgFile, err := os.Open(file)
	if err != nil {
		return nil, "", err
	}
	defer imgFile.Close()
	img, format, err := image.Decode(imgFile)
	if err != nil {
		return nil, "", err
	}
	return img, format, nil
}

// ConcatTwoImages stitches two images and writes the result to output
func ConcatTwoImages(second, first image.Image, offset image.Point, filename string) error {
	fmt.Printf("[ConcatTwoImages] first: %v\tsecond: %v\tpos: %v\toutput: %s\n", first.Bounds(), second.Bounds(), offset, filename)
	b := first.Bounds()
	image3 := image.NewRGBA(b)
	draw.Draw(image3, b, first, image.ZP, draw.Src)
	draw.Draw(image3, second.Bounds().Add(offset), second, image.ZP, draw.Over)

	third, err := os.Create(filename)
	if err != nil {
		log.Fatalf("failed to create: %s", err)
	}
	jpeg.Encode(third, image3, &jpeg.Options{Quality: jpeg.DefaultQuality})
	defer third.Close()
	return err
}

// GetDataFromTemplateImage parses template name and returns amount and currency in the name
func GetDataFromTemplateImage(filename string) (amount float64, currency string, err error) {
	fmt.Printf("[GetDataFromTemplateImage] filename: %v\n", filename)
	parts := strings.Split(filename, "-")
	if len(parts) < 2 {
		return 0, "", fmt.Errorf("filename has no extension")
	}
	// fmt.Printf("[0] parts: %+v\n", parts)
	// parts = strings.Split(parts[0], "-")
	fmt.Printf("[1] parts: %+v\n", parts)
	for _, part := range parts {
		if len(part) == 0 {
			fmt.Printf("\tSkipping this part\n")
			continue
		}
		valF64, err := strconv.ParseFloat(part, 64)
		if err == nil && valF64 > 0 {
			amount = valF64
		} else if len(part) == 3 {
			currency = part
		}
		fmt.Printf("\tpart: %v\tvalF64: %v\tAmount: %v\tCurrency: %v\nError: %v\n", part, valF64, amount, currency, err)
	}
	if amount > 0 && currency != "" {
		return amount, currency, nil
	}
	return amount, currency, fmt.Errorf("either currency or amount is missing from the filename")
}

// AddTextToImage takes an image pointer and sets text on the offset position
func AddTextToImage(img image.Image, offset image.Point, txt string) image.Image {
	ctx := gg.NewContextForImage(img)
	ctx.SetRGB(0, 0, 0)
	ctx.DrawString(txt, float64(offset.X), float64(offset.Y))
	return ctx.Image()
}

package models

import (
	"bytes"
	"fmt"
	"image"
	"image/color"
	"strings"

	qrcode "github.com/skip2/go-qrcode"
	"gopkg.in/mgo.v2/bson"
)

// QRCode represents a data from QR code or a data that would be converted to QR code
type QRCode struct {
	FullURL         string            `json:"full_url" bson:"full_url"`
	BaseURL         string            `json:"base_url" bson:"base_url"`
	RequestType     string            `json:"request_type" bson:"request_type"`
	ItemID          bson.ObjectId     `json:"item_id" bson:"item_id"`
	ExtraParameters map[string]string `json:"extra_parameters" bson:"extra_parameters"`
}

// ToURL converts current object to full url
func (q QRCode) ToURL() string {
	url := "/"
	params := []string{}
	if len(q.BaseURL) > 0 && (strings.HasPrefix(q.BaseURL, "https") || strings.HasPrefix(q.BaseURL, "http")) {
		url = q.BaseURL
	}
	if len(q.RequestType) > 0 {
		params = append(params, fmt.Sprintf("request=%v", q.RequestType))
	}
	if len(q.ItemID) > 0 {
		params = append(params, fmt.Sprintf("item=%v", q.ItemID.Hex()))
	}

	for key, val := range q.ExtraParameters {
		if len(key) > 0 && val != "" {
			params = append(params, fmt.Sprintf("%s=%v", key, val))
		}
	}

	return fmt.Sprintf("%s?%s", url, strings.Join(params, "&"))
}

// FromURL converts current object to full url
func (q *QRCode) FromURL(url string) error {
	parts := strings.Split(url, "?")
	if len(parts) > 0 && (strings.HasPrefix(url, "https") || strings.HasPrefix(url, "http")) {
		q.BaseURL = parts[0]
	} else if len(parts) < 2 {
		return fmt.Errorf("no enough data to construct url")
	}

	params := []string{}

	if len(parts) > 1 {
		params = strings.Split(parts[1], "&")
	}

	for _, param := range params {
		parts = strings.Split(param, "=")
		if len(parts) < 2 {
			continue
		}
		key := parts[0]
		value := parts[1]

		if key == "" {
			continue
		} else if key == "request" {
			q.RequestType = value
		} else if key == "item" {
			q.ItemID = bson.ObjectIdHex(value)
		} else {
			q.ExtraParameters[key] = value
		}
	}

	return nil
}

// SaveToFile saves qr data an image
func (q QRCode) SaveToFile(file string, qrOffset image.Point, templatePath, expirationDate string) error {
	if len(file) == 0 {
		return fmt.Errorf("missing file path parameter")
	}
	hasBaseURL := strings.HasPrefix(q.BaseURL, "http") || strings.HasPrefix(q.BaseURL, "https")
	hasParams := len(q.RequestType) > 0 || len(q.ItemID.Hex()) == 24 || q.ExtraParameters != nil
	if !hasBaseURL && !hasParams {
		return fmt.Errorf("either base url or parameters is required to generate qr image")
	}
	if len(templatePath) > 0 {
		tplImg, format, err := OpenImage(templatePath)
		if err != nil {
			return err
		}

		tplImg = AddTextToImage(tplImg, image.Pt(147, 570), fmt.Sprintf("Expires: %v", expirationDate))

		fmt.Printf("Template format: %s\n", format)

		qr, err := qrcode.New(q.ToURL(), qrcode.Medium)
		if err != nil {
			return err
		}

		qr.ForegroundColor = color.RGBA{R: uint8(6), G: uint8(42), B: uint8(58), A: uint8(255)}
		qr.BackgroundColor = color.Transparent

		var png []byte
		png, err = qr.PNG(256) // qrcode.Encode(q.ToURL(), qrcode.Medium, 256)
		if err != nil {
			return err
		}

		qrImage, format, err := image.Decode(bytes.NewReader(png))
		if err != nil {
			return err
		}
		fmt.Printf("QR Image format: %s\n", format)

		err = ConcatTwoImages(qrImage, tplImg, qrOffset, file)
		if err != nil {
			return err
		}
		fmt.Printf("[ConcatTwoImages] file: %+v\n", file)
		return nil
	}
	return qrcode.WriteColorFile(q.ToURL(), qrcode.Medium, 256, color.Transparent, color.Black, file)
}

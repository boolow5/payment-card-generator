package utils

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"hash/fnv"
)

// HashUInt32 generates unique hash number
func HashUInt32(s string) uint32 {
	h := fnv.New32a()
	h.Write([]byte(s))
	return h.Sum32()
}

// Hash encodes HMAC sha1 hash
func Hash(key, secret string) string {
	h := hmac.New(sha256.New, []byte(secret))
	h.Write([]byte(key))

	return hex.EncodeToString(h.Sum(nil))
}
